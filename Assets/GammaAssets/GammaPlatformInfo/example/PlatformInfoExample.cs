﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Gamma.PlatformInfo;

public class PlatformInfoExample : MonoBehaviour {

	public Text versionTex;
	public Text shortVersionText;
	public Text productNameText;
	public Text deviceIdText;

	// Use this for initialization
	void Start () {
	
		versionTex.text = "Bundle Version: " + PlatformInfo.GetBundleVersion();
		shortVersionText.text = "Bundle Short Version:: " + PlatformInfo.GetPublicBundleVersion();
		productNameText.text = "Display Name: " + PlatformInfo.GetDisplayName();
		deviceIdText.text = "DeviceId: " + PlatformInfo.GetDeviceId();
	}
}
