﻿#if UNITY_IPHONE
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace Gamma.PlatformInfo.NativeBinding{
	public class iOSPlatformInfoBinding : IPlatformInfo {
		//[NSString stringWithFormat:@"Version %@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"], kRevisionNumber]
		
		[DllImport ("__Internal")]
		private static extern string _getBundleVersion();
		
		public string GetInternalVersion()
		{
#if !UNITY_EDITOR
			return _getBundleVersion();
#else
			return "Editor_ " + UnityEditor.PlayerSettings.bundleVersion;
#endif
		}
		
		[DllImport ("__Internal")]
		private static extern string _getBundleShortVersion();
		
		public string GetPublicVersion()
		{
#if !UNITY_EDITOR
			return _getBundleShortVersion();
#else
			return "Editor_ " + UnityEditor.PlayerSettings.bundleVersion;
#endif
		}
	}
}
#endif
