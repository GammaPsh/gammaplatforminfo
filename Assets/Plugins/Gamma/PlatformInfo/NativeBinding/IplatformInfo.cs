﻿using UnityEngine;
using System.Collections;

namespace Gamma.PlatformInfo.NativeBinding{
	public interface IPlatformInfo {

		string GetInternalVersion();
		string GetPublicVersion();

	}
}
