﻿using UnityEngine;
using System.Collections;


#if UNITY_ANDROID
namespace Gamma.PlatformInfo.NativeBinding{
	public class AndroidPlatformInfoBinding : IPlatformInfo
	{

		public string GetInternalVersion()
		{
#if !UNITY_EDITOR
			AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject joActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject joPackageManager = joActivity.Call<AndroidJavaObject>("getPackageManager");
			AndroidJavaObject joPackageName = joActivity.Call<AndroidJavaObject>("getPackageName");
			AndroidJavaObject joPackageInfo = joPackageManager.Call<AndroidJavaObject>("getPackageInfo", joPackageName, 0);
			int versionCode = joPackageInfo.Get<int>("versionCode");
			return versionCode.ToString();
#else
			return "Editor_ " + UnityEditor.PlayerSettings.bundleVersion;
#endif
		}

		public string GetPublicVersion()
		{
			return Application.version;
		}

		public string GetDisplayName()
		{
			return Application.productName;
		}
	}
}
#endif
