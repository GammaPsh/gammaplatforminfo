﻿#if UNITY_STANDALONE || UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JsonFx.Json;

namespace Gamma.PlatformInfo.NativeBinding{
	public class StandalonePlatformInfoBinding : IPlatformInfo {

		Dictionary<string, object> infoDictionary;
		public StandalonePlatformInfoBinding()
		{
			string platformInfoPath = Path.Combine(Application.streamingAssetsPath, "platforminfo.txt");

			if(File.Exists(platformInfoPath)){
				infoDictionary = JsonReader.Deserialize<Dictionary<string, object>>(File.ReadAllText(platformInfoPath));
			}else{
				infoDictionary = new Dictionary<string, object>();
			}
		}

#region IPlatformInfo implementation
		public string GetInternalVersion ()
		{
			if(infoDictionary.ContainsKey("buildNumber")){
				return System.Convert.ToString(infoDictionary["buildNumber"]);
			}
			return "unavailable";
		}
		public string GetPublicVersion ()
		{
			return Application.version;
		}
#endregion

#if UNITY_EDITOR
		public static void SaveEditorInfo(string path)
		{
			Dictionary<string, object> saveDictionary = new Dictionary<string, object>();
			saveDictionary.Add("buildNumber", UnityEditor.PlayerSettings.iOS.buildNumber);

			string platformInfoPath = Path.Combine(path, "StreamingAssets");

			if(!Directory.Exists (platformInfoPath)){
				Directory.CreateDirectory(platformInfoPath);
			}

			File.WriteAllText(Path.Combine(platformInfoPath, "platforminfo.txt"), JsonWriter.Serialize(saveDictionary));
		}
#endif
	}
}
#endif
