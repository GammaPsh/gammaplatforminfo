﻿using UnityEngine;
using System.Collections;

namespace Gamma.PlatformInfo.NativeBinding {
	public class WebplayerPlatformInfoBinding : IPlatformInfo {

			#region IPlatformInfo implementation

			public string GetInternalVersion ()
			{
				return "undfined";
			}

			public string GetPublicVersion ()
			{
				return Application.version;
			}

			#endregion
	}
}
