﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using Gamma.PlatformInfo.NativeBinding;

namespace Gamma.PlatformInfo{
	public class PlatformInfo {

		private static IPlatformInfo platformInfo;
		private static IPlatformInfo GetPlatformInfoObject()
		{
			if(platformInfo != null)
				return platformInfo;

#if UNITY_IPHONE
			platformInfo = new iOSPlatformInfoBinding();
#elif UNITY_ANDROID
			platformInfo = new AndroidPlatformInfoBinding();
#elif UNITY_STANDALONE
			platformInfo = new StandalonePlatformInfoBinding();
#elif UNITY_WEBPLAYER
			platformInfo = new WebplayerPlatformInfoBinding();
#endif
			return platformInfo;
		}

		public static string GetBundleVersion()
		{
			return GetPlatformInfoObject().GetInternalVersion();
		}

		public static string GetPublicBundleVersion()
		{
			return GetPlatformInfoObject().GetPublicVersion();
		}

		public static string GetDisplayName()
		{
			return Application.productName;
		}

		public static string GetDeviceId()
		{
			string deviceId = PlayerPrefs.GetString("GammaPlatformInfo.deviceid");
			if(string.IsNullOrEmpty(deviceId)){
				deviceId = System.Guid.NewGuid().ToString();
				PlayerPrefs.SetString("GammaPlatformInfo.deviceid", deviceId);
				PlayerPrefs.Save();
			}
			return deviceId;
		}
	}
}
